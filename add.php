<?php
header('Access-Control-Allow-Origin: *');
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");

include '../../essentials/connection.php';

$conn = new Connection();

if(!$conn->connect()) die('Configuration error');
//else echo 'successful connection'; //must delete this upon finalization

$to_decode = json_decode(file_get_contents("php://input"));
$username = $to_decode->username;
$password = $to_decode->password;
$ID = $to_decode->ID;
$name = $to_decode->name;
$address = $to_decode->address;
$contact = $to_decode->contact;

$password = password_hash($password, PASSWORD_DEFAULT);

$sql = "INSERT INTO user_tbl (username, password, ID, name, address, contact) VALUES ('$username', '$password', '$ID', '$name', '$address', '$contact')";

if(mysqli_query($conn->connect(), $sql)){
    http_response_code(200);
    echo json_encode(array('msg' => 'success', 'key' => $username));
} else {
    http_response_code(403);
    echo json_encode(array('msg' => 'failed', 'response' => mysqli_error($conn->connect())));
}

$conn->close($conn->connect());
?>